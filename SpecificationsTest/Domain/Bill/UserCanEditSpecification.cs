﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Specifications;
using SpecificationsTest.Domain.User;

namespace SpecificationsTest.Domain.Bill {


    /// <summary>
    /// Incapsulating bill changing policy by user role
    /// </summary>
    public class UserCanEditSpecification : ISpecification<Bill> {
        private User.User _editor;

        private UserCanEditSpecification() {

        }

        public UserCanEditSpecification( User.User editor ) {
            _editor = editor;
        }

        public bool IsSatisfiedBy( Bill candidate ) {

            //Если оплата отправленна или счет получен , для закрытия
            if ( candidate.State == BillState.SendPayment || candidate.State == BillState.PaymentRequested ) {
                return _editor.Role == Roles.Executor;
            }
            if ( candidate.State == BillState.SendRequest ) { //Если в статусе запрошен
                return _editor.Role == Roles.Customer;
            }
            if ( candidate.State == BillState.New && _editor.Role == Roles.Executor ) {
                return true;
            }
            return false;
        }
    }
}
