﻿/// <summary>
/// Статусы счета
/// </summary>
public enum BillState {
    /// <summary>
    /// Новый счет
    /// </summary>
    New,
    /// <summary>
    /// Запрошен
    /// </summary>
    SendRequest,
    /// <summary>
    /// Оплата отправлена
    /// </summary>
    SendPayment,
    /// <summary>
    /// Счет получен
    /// </summary>
    PaymentRequested
}