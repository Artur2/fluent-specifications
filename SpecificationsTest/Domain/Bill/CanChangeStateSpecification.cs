﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Specifications;

namespace SpecificationsTest.Domain.Bill {

    /// <summary>
    /// Incapsulating bill changing policy by state
    /// </summary>
    public class CanChangeStateSpecification : ISpecification<Bill> {

        private BillState _stateToChange;

        public CanChangeStateSpecification( BillState stateToChange ) {
            _stateToChange = stateToChange;
        }

        public bool IsSatisfiedBy( Bill candidate ) {
            
            //Если оплата отправленна то исполнитель может подтвердить счет
            if ( candidate.State == BillState.SendPayment && _stateToChange == BillState.PaymentRequested ) {
                return true;
            }
            //Если счет запрошен, то кастомер может его поменять на оплата отправленна
            if ( candidate.State == BillState.SendRequest && _stateToChange == BillState.SendPayment ) {
                return true;
            }
            if ( candidate.State == BillState.New && _stateToChange == BillState.SendRequest ) {
                return true;
            }
            return false;
        }
    }
}
