﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationsTest.Domain.Bill {
    public static class BillFactory {

        public static Bill CreateNew() {
            return new Bill() { State = BillState.New };
        }
    }
}
