﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Specifications;

namespace SpecificationsTest.Domain.Bill {

    /// <summary>
    /// Clean rich domain model
    /// </summary>
    public class Bill {

        public BillState State { get; set; }

        private bool TryEdit( User.User editor, BillState state, out string resultMessage ) {

            var userCanEditSpec = new UserCanEditSpecification( editor );
            var userCanChangeSpec = new CanChangeStateSpecification( state );
            if ( userCanEditSpec.And( userCanChangeSpec ).IsSatisfiedBy( this ) ) {
                State = state;
                resultMessage = "State changed to " + state.ToString();
                return true;
            }

            resultMessage = "This user cant edit";
            return false;
        }

        public bool TrySendPayment( User.User editor, out string resultMessage ) {
            return TryEdit( editor, BillState.SendPayment, out resultMessage );
        }

        public bool TryChangeToPaymentRequested( User.User editor, out string resultMessage ) {
            return TryEdit( editor, BillState.PaymentRequested, out resultMessage );
        }

        public bool TrySendRequest( User.User editor, out string resultMessage ) {
            return TryEdit( editor, BillState.SendRequest, out resultMessage );
        }
    }
}
