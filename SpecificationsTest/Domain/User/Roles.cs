﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationsTest.Domain.User {

    /// <summary>
    /// Роли пользователя
    /// </summary>
    public enum Roles {
        /// <summary>
        /// Заказчик
        /// </summary>
        Customer,
        /// <summary>
        /// Исполнитель
        /// </summary>
        Executor
    }
}
