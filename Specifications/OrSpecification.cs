﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specifications {

    internal class OrSpecification<TEntity> : ISpecification<TEntity> where TEntity : class {

        private readonly ISpecification<TEntity> specification1, specification2;

        public bool IsSatisfiedBy( TEntity candidate ) {
            return specification1.IsSatisfiedBy( candidate ) || specification2.IsSatisfiedBy( candidate );
        }

        internal OrSpecification( ISpecification<TEntity> specification1, ISpecification<TEntity> specification2 ) {
            this.specification1 = specification1;
            this.specification2 = specification2;
        }
    }
}
