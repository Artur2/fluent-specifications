﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Specifications;
using SpecificationsTest.Domain.Bill;
using SpecificationsTest.Domain.User;

namespace SpecificationsTest {

    [TestClass]
    public class SpecificationTest {

        [TestMethod]
        public void CustomerSendPayment() {

            var user = new User() { Role = Roles.Customer }; //Заказчик
            var billToEdit = new Bill() { State = BillState.SendRequest }; //Счет в состоянии "Запрос оплаты"

            var userCanEditSpec = new UserCanEditSpecification( user );
            var billCanChangeStateSpec = new CanChangeStateSpecification( BillState.SendPayment );

            Assert.IsTrue( userCanEditSpec.And( billCanChangeStateSpec ).IsSatisfiedBy( billToEdit ) );
        }

        [TestMethod]
        public void ExecutorRequestingPayment() {

            var user = new User() { Role = Roles.Executor }; //Исполнитель
            var billToEdit = BillFactory.CreateNew(); //Счет в состоянии новый

            var userCanEdit = new UserCanEditSpecification( user );
            var billCanChangeStateSpec = new CanChangeStateSpecification( BillState.SendRequest );

            Assert.IsTrue( userCanEdit.And( billCanChangeStateSpec ).IsSatisfiedBy( billToEdit ) );
        }

        [TestMethod]
        public void IncapsulatedTest() {

            var user = new User() { Role = Roles.Executor };
            var bill = new Bill() { State = BillState.SendPayment };
            var message = string.Empty;
            var success = bill.TryChangeToPaymentRequested( user,out message );
            Assert.IsTrue( success );
        }
    }

}
