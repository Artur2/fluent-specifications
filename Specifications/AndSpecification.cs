﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specifications {
    internal class AndSpecification<TEntity> : ISpecification<TEntity> where TEntity : class {

        private readonly ISpecification<TEntity> specification1, specification2;

        public bool IsSatisfiedBy( TEntity candidate ) {
            return specification1.IsSatisfiedBy( candidate ) && specification2.IsSatisfiedBy( candidate );
        }

        internal AndSpecification( ISpecification<TEntity> left, ISpecification<TEntity> right ) {
            specification1 = left;
            specification2 = right;
        }
    }
}
