﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specifications {

    internal class NotSpecification<TEntity> : ISpecification<TEntity> where TEntity : class {

        private readonly ISpecification<TEntity> wrapped;

        public bool IsSatisfiedBy( TEntity candidate ) {
            return !wrapped.IsSatisfiedBy( candidate );
        }

        public NotSpecification( ISpecification<TEntity> candidate ) {
            wrapped = candidate;
        }
    }
}
