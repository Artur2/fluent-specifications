﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specifications {

    public static class SpecificationExtension {

        public static ISpecification<TEntity> And<TEntity>( this ISpecification<TEntity> left, ISpecification<TEntity> right ) where TEntity : class {
            return new AndSpecification<TEntity>( left, right );
        }

        public static ISpecification<TEntity> Or<TEntity>( this ISpecification<TEntity> left,
                                                           ISpecification<TEntity> right ) where TEntity : class {
            return new OrSpecification<TEntity>( left, right );
        }

        public static ISpecification<TEntity> Not<TEntity>( this ISpecification<TEntity> wrapped ) where TEntity : class {
            return new NotSpecification<TEntity>( wrapped );
        }
    }
}
